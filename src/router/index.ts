import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "Acceuil",
      component: () => import("../views/Acceuil.vue")
    },
    {
      path: "/inventaire",
      name: "inventaire",
      component: () => import("../views/Inventaire.vue")
    }
  ],
});

export default router;
